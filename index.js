require("dotenv").config();
const axios = require("axios");

const url = "https://www.andjoy.life/es/";

async function main() {
  try {
    const daysInAdvance = process.env.DAYS_IN_ADVANCE;
    const hourToBook = process.env.HOUR;
    const secondsToWait = 5;

    var targetDateTime = new Date();
    targetDateTime.setHours(hourToBook);
    targetDateTime.setMinutes(0);
    targetDateTime.setSeconds(0);

    var waitForBookTime = function () {
      var currentDate = new Date();
      var intr = setInterval(async function () {
        currentDate = new Date();
        console.log(
          `Waiting ${secondsToWait} seconds to check again! Current ${currentDate.toLocaleString()} - Target ${targetDateTime.toLocaleString()}`
        );
        if (currentDate >= targetDateTime) {
          clearInterval(intr);

          var fullBookDate = new Date(
            Date.now() + 3600 * 1000 * 24 * daysInAdvance
          );

          var dateWithoutTime = fullBookDate.toISOString().substr(0, 10);

          console.log(
            `Trying to a book a class for ${dateWithoutTime} at ${hourToBook}. Current time: ${new Date().toLocaleString()}`
          );

          const getEventsParams = createGetEventsParams(
            dateWithoutTime,
            hourToBook
          );

          const classesResult = await axios.post(
            `${url}events?isWidget=true`,
            getEventsParams,
            config
          );
          var event = classesResult.data.classes.list[0];
          var eventId = event.classBase.id;
          var bookingId = event.classBase.bookingId;
          var couponId = event.multigymUser.couponId;
          console.log(
            `Event ${eventId} retrieved. Coupon Id ${couponId}. Booking Id ${bookingId}`
          );

          if (bookingId) {
            console.log("Class already booked");
            return;
          }

          const bookParams = new URLSearchParams({
            eventId: eventId,
            bookDate: dateWithoutTime,
            couponId: couponId,
          });

          const bookResult = await axios.post(
            `${url}bookings/book`,
            bookParams,
            config
          );
          console.log(`Call result: ${bookResult.data}`);
        }
      }, secondsToWait * 1000);
    };

    waitForBookTime();
  } catch (error) {
    console.log(`ERROR => ${error.message}`);
  }
}

Date.prototype.addDays = function (days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

function createGetEventsParams(bookDate, hourToBook) {
  const getEventsParams = new URLSearchParams({
    city: "barcelona",
    gym: "60e58030d67d3c3a4d9875c7",
    date: bookDate,
    initHour: hourToBook,
    endHour: hourToBook,
    page: 0,
    bookable: false,
    onlyChannels: false,
    timeZoneId: "Europe/Madrid",
  });
  return getEventsParams;
}

const config = {
  headers: {
    "X-Requested-With": "XMLHttpRequest",
    "Sec-GPC": "1",
    "Sec-Fetch-Site": "same-origin",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Dest": "empty",
    Cookie: process.env.COOKIE,
  },
};

main();
