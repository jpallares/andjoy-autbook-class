# AndJoy Autobook class

Node script that tries to book an AndJoy class for a **given hour** with **given days** in advance.

### How to use

Configure the environment variables following `.env-example`.
Configure a schedule build that fits your needs. In my case, since I want it to run at 19 every SAT,SUN,MON,TUE. The cron job is `0 19 * * 6,0,1,2`.
